package com.collabera.teamtriveni.repository;

import com.collabera.teamtriveni.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {


}
