package com.collabera.teamtriveni.repository;

import com.collabera.teamtriveni.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
