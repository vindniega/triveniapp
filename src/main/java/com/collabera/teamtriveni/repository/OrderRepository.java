package com.collabera.teamtriveni.repository;

import com.collabera.teamtriveni.model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {
}
