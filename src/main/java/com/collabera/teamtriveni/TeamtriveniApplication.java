package com.collabera.teamtriveni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamtriveniApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeamtriveniApplication.class, args);
	}

}
