package com.collabera.teamtriveni.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue
    private Long id;
    private String productName;
    private String productType;
    private Double price;
    private String description;
    private Integer qty;
    private String imageLink;

    @ManyToMany(targetEntity = User.class,cascade = CascadeType.ALL )
    private List<User> users;

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private Set<Order> order;
}
