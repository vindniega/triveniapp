package com.collabera.teamtriveni.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue
    private Long id;
    private String username;
    private String password;

    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private Set<Order> orders;

    @ManyToMany(targetEntity = Product.class, mappedBy = "users", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Product> products;
}
