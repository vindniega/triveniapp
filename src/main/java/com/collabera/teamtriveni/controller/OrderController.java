package com.collabera.teamtriveni.controller;

import com.collabera.teamtriveni.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/{userID}/{productID}")
    public ResponseEntity<Object> createOrder(@PathVariable Long userID, @PathVariable Long productID){
        if(orderService.order(userID, productID)) return new ResponseEntity<>("Ordered!", HttpStatus.OK);
        return new ResponseEntity<>("Out of stock", HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<Object> allOrders(){
        return new ResponseEntity<>(orderService.getAllOrders(), HttpStatus.OK);
    }

    @GetMapping("/{userID}")
    public ResponseEntity<Object> getOrder(@PathVariable Long userID){
        return new ResponseEntity<>(orderService.getOrderFromUser(userID), HttpStatus.OK);
    }

    @DeleteMapping("/{orderID}")
    public ResponseEntity<Object> orderDelete(@PathVariable Long orderID){
        orderService.deleteOrder(orderID);
        return new ResponseEntity<>("Deleted!", HttpStatus.OK);
    }
}
