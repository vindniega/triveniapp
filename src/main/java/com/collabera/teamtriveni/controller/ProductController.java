package com.collabera.teamtriveni.controller;

import com.collabera.teamtriveni.model.Product;
import com.collabera.teamtriveni.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/")
    public ResponseEntity<Object> productCreate(@RequestBody Product product){
        productService.createProduct(product);
        return new ResponseEntity<>("Created!", HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<Object> getProducts(){
        return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
    }
}
