package com.collabera.teamtriveni.controller;

import com.collabera.teamtriveni.model.User;
import com.collabera.teamtriveni.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/")
    public ResponseEntity<Object> createUser(@RequestBody User user){
        userService.registerUser(user);
        return new ResponseEntity<>("Created!", HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

}
