package com.collabera.teamtriveni.service;

import com.collabera.teamtriveni.model.Order;

import java.util.stream.Stream;

public interface OrderService {

    boolean order(Long userID, Long productId);
    Iterable<Order> getAllOrders();
    Stream<Order> getOrderFromUser(Long userID);
    void deleteOrder(Long orderID);
}
