package com.collabera.teamtriveni.service;

import com.collabera.teamtriveni.model.Order;
import com.collabera.teamtriveni.model.Product;
import com.collabera.teamtriveni.model.User;
import com.collabera.teamtriveni.repository.OrderRepository;
import com.collabera.teamtriveni.repository.ProductRepository;
import com.collabera.teamtriveni.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;


@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public boolean order(Long userID, Long productId) {
        User userOrder = userRepository.findById(userID).get();
        Product productOrder = productRepository.findById(productId).get();
        if(productOrder.getQty() == 0) return false;

        Order newOrder = new Order();
        newOrder.setUser(userOrder);
        newOrder.setProduct(productOrder);
        productOrder.getUsers().add(userOrder);
        productOrder.setQty(productOrder.getQty() - 1);
        productRepository.save(productOrder);
        orderRepository.save(newOrder);
        return true;
    }

    @Override
    public Iterable<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public Stream<Order> getOrderFromUser(Long userID){
        return Streamable.of(getAllOrders()).toList().stream().filter(element -> element.getUser().getId() == userID);
    }

    @Override
    public void deleteOrder(Long orderID) {
        orderRepository.deleteById(orderID);
    }
}
