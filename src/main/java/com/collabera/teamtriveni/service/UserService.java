package com.collabera.teamtriveni.service;

import com.collabera.teamtriveni.model.User;

public interface UserService {

    void registerUser(User user);
    Iterable<User> getAllUsers();
}
