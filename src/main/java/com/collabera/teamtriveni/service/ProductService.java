package com.collabera.teamtriveni.service;

import com.collabera.teamtriveni.model.Product;

public interface ProductService {

    void createProduct(Product product);
    Iterable<Product> getAllProducts();
}
